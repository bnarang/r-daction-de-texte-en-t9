import java.lang.Object;
import java.lang.String;
import java.util.ArrayList;

public class Tree {

  /* ********************************* */
  /* **** Les variables de classe **** */
  /* ********************************* */

  private Tree mom, lokid, eqkid, hikid; // liens vers le parent et les trois fils - Notons que mom n'est finalement utile à rien
  private int node; // valeur sur le noeud donc entre 2 et 9
  private boolean leaf; // true si l'arbre est une feuille i.e. il n'a pas de fils
  private ArrayList<String> words; // mot correspondant à la séquence aboutissant à ce noeud

  /* *************************** */
  /* **** Les constructeurs **** */
  /* *************************** */

  // Constructeur à n'utiliser que pour créer la racine de l'arbre
  public Tree(int newNode) {
    mom   = null;
    lokid = null;
    eqkid = null;
    hikid = null;
    node  = newNode;
    leaf  = true;
    words = new ArrayList<String>();
    Tree newLeaf = new Tree(newNode, this);
    this.addChild(newLeaf);
  };

  public Tree(int newNode, Tree parent) {
    mom   = parent;
    lokid = null;
    eqkid = null;
    hikid = null;
    node  = newNode;
    leaf  = true;
    words = new ArrayList<String>();
  };

  /* ********************************* */
  /* **** Les fonctions de classe **** */
  /* ********************************* */

  // Ajouter un fils à la bonne place
  public void addChild(Tree newLeaf){
    if ( node > newLeaf.getNode() )
      lokid = newLeaf;
    if ( node == newLeaf.getNode() )
      eqkid = newLeaf;
    if ( node < newLeaf.getNode() )
      hikid = newLeaf;
    leaf = false;
  }

  // Ajout du mot newWord - index retient à quelle lettre on se trouve dans le mot (important car la fonction est récursive)
  public void addWord(String newWord, int index) {
    char letter = newWord.charAt(index);
    int letter_int = Function.giveCorrespondingNumber(letter);

    // si c'est la dernière lettre
    if (index == newWord.length()-1 && letter_int == node) {
      // On ajoute le mot à words
      words.add(newWord);
      // System.out.println("Word " + newWord + " successfully added.");
    }
    else {
      if (letter_int < node) {
        if (lokid == null){
          Tree newLeaf = new Tree(letter_int, this);
          this.addChild(newLeaf);
        }
        lokid.addWord(newWord, index);
      }
      else if (letter_int > node) {
        if (hikid == null) {
          Tree newLeaf = new Tree(letter_int, this);
          this.addChild(newLeaf);
        }
        hikid.addWord(newWord, index);
      }
      else {
        if (eqkid == null) {
          Tree newLeaf = new Tree(letter_int, this);
          this.addChild(newLeaf);
        }
        eqkid.addWord(newWord, index+1);
      }
    }
  }

  // Se déplacer dans l'arbre pour trouver le prochain noeud égale à keyboardInput
  public Tree search(int keyboardInput) {
    // à gauche : on continue la recherche
    if (keyboardInput > node && hikid != null)
      return hikid.search(keyboardInput);
    // à droite : on continue la recherche
    else if (keyboardInput < node && lokid != null)
      return lokid.search(keyboardInput);
    // si on ne peut plus descendre dans l'arbre,
    // i.e. la séquence de chiffres ne représente aucun mot,
    // on renvoie le dernier noeud atteint
    else
      return this;
  }

  public int getNode() { return node; }

  // Obtenir mots du noeud
  public ArrayList getWords() {
    if (words.size() > 0)

      return words;
    else {
      ArrayList<String> tmp_array = new ArrayList<String>();
      tmp_array.add(" ");
      return tmp_array;
    }
  }

  // Permuter liste de mots
  public void switchWords() {
    // On 'rotationne' la liste :
    // [1, 2, 3, 4] -> [2, 3, 4, 1]
    if ( !words.isEmpty() ) {
      String tmp = (String)words.remove(0);
      words.add(tmp);
    }
  }

  public Tree getEqkid(){ return eqkid; }
}

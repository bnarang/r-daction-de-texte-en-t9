import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.EmptyBorder;
import java.util.ArrayList;


public class MainWindow extends JFrame {

  /* ********************************* */
  /* **** Les variables de classe **** */
  /* ********************************* */

  // Le container global
  private JPanel container = new JPanel();
  // Tableau des touches, et des boutons
  private String[] key_numbers_string = {"", "suiv.", "<-", "1", "2", "3", "4", "5", "6", "7", "8", "9", "*", "0", "#"};
  private String[] key_letters_string = {"", "", "", ".", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz", "Aa", "", "space"};
  private String[] key_case           = {"Aa", "aa", "AA"};
  private String[] ponctuation        = {".", ",", "!", "?"};
  private JButton[] key_button = new JButton[key_numbers_string.length]; // L'ensemble des touches
  private Dimension key_dim    = new Dimension(68, 40); // Taille d'une touche
  // L'écran
  private JTextArea screen = new JTextArea(); // Partie écran
  // Variables
  private int ponctuation_idx = -1; // Indice du signe de ponctuation courant ; -1 si pas de signe de ponctuation courant.
  private Tree tree = null; // L'arbre courant
  private Tree root = null; // L'arbre racine entier
  private String phrase = ""; // La phrase à l'écran
  private ArrayList<Integer> phrase_list = new ArrayList<Integer>(); // La suite de chiffre tapée depuis le début
  private ArrayList<Integer> current_word_list = new ArrayList<Integer>(); // La suite de chiffre tapée sur le mot courant
  private String current_word_str = ""; // Le mot courant
  private int keyboard_input;
  private int last_keyboard_input;
  private int case_mode = 0; // case_mode = 0 <=> Bla / case_mode = 1 <=> bla / case_mode = 2 <=> BLA


  /* *************************** */
  /* **** Les constructeurs **** */
  /* *************************** */

  // Initialisation de la fenêtre, des touches
  public MainWindow(Tree theRoot) {
    // Fenêtre principale
    this.setSize(240, 380);
    this.setTitle("SuperT9");
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setLocationRelativeTo(null);
    this.setResizable(false);
    // On initialise le conteneur avec tous les composants
    initComponent();
    // On ajoute le conteneur
    this.setContentPane(container);
    this.setVisible(true);
    tree = theRoot;
    root = theRoot;
  }


  /* ********************************* */
  /* **** Les fonctions de classe **** */
  /* ********************************* */

  private void updateScreen() {
    String tmp = phrase.concat(current_word_str);
    screen.setText(tmp);
  }

  // Création de l'interface graphique
  private void initComponent() {
    // Partie écran
    Font police = new Font("Arial", Font.BOLD, 20);
    screen      = new JTextArea("");
    screen.setFont         (police);
    screen.setPreferredSize( new Dimension(210, 100) );
    screen.setEditable     (false);
    screen.setCursor       (null);
    screen.setOpaque       (false);
    screen.setFocusable    (false);
    screen.setLineWrap     (true);
    screen.setWrapStyleWord(true);
    // La partie clavier
    JPanel keyboard_pan = new JPanel();
    keyboard_pan.setPreferredSize( new Dimension(220, 300) );
    // La partie écran
    JPanel screen_pan   = new JPanel();
    screen_pan.setPreferredSize( new Dimension(220, 100) );

    for(int i = 0; i < key_numbers_string.length; i++) {
      key_button[i] = new JButton();
      key_button[i].setLayout( new BorderLayout() );
      key_button[i].add( BorderLayout.NORTH, new JLabel(key_numbers_string[i]) );
      key_button[i].add( BorderLayout.SOUTH, new JLabel(key_letters_string[i]) );
      key_button[i].setName( String.valueOf(i) );
      key_button[i].setForeground(Color.BLACK);
      key_button[i].setBackground(Color.WHITE);
      Border line     = new LineBorder(Color.BLACK);
      Border margin   = new EmptyBorder(5, 5, 5, 5);
      Border compound = new CompoundBorder(line, margin);
      key_button[i].setBorder(compound);
      key_button[i].setPreferredSize(key_dim);
      key_button[i].addActionListener( new KeyboardButton() );
      keyboard_pan.add(key_button[i]);
    }
    screen_pan.add(screen);
    screen_pan.setBorder( BorderFactory.createLineBorder(Color.black) );
    container.add(screen_pan, BorderLayout.NORTH);
    container.add(keyboard_pan, BorderLayout.CENTER);
  }

  // Majuscule sur la première lettre
  private String capitalize(String str) {
    if (str.length() > 0)
      return str.substring(0, 1).toUpperCase().concat(str.substring(1).toLowerCase());
    else
      return "";
  }
  // Mettre en forme la bonne casse
  private String render_case(String str) {
    switch (case_mode) {
      case 0: // Première lettre en majuscule
        return capitalize(current_word_str);
      case 1: // Tout en minuscules
        return current_word_str.toLowerCase();
      case 2: // Tout en majuscules
        return current_word_str.toUpperCase();
      default:
        return "";
    }
  }


  /* ****************************** */
  /* **** Les classes internes **** */
  /* ****************************** */

  // Listener des touches
  class KeyboardButton implements ActionListener {
    public void actionPerformed(ActionEvent e) {
      // On récupère la touche correspondante
      int keyboard_input = Integer.parseInt( ( (JButton)e.getSource() ).getName() );

      // Si on tape une nouvelle touche de texte
      if (keyboard_input > 3 && keyboard_input < 12) {
        if (tree.getEqkid() != null) {
          // On actualise l'arbre
          tree = tree.getEqkid().search(keyboard_input-2);
          // On actualise l'affichage
          current_word_str = (String)tree.getWords().get(0);
        }
        //Pour afficher quand même quelque chose quand la séquence ne correspond à rien
        if (current_word_str == " " || tree.getEqkid() == null) {
          String tmp = Function.giveLetter( (int)current_word_list.get(0));
          for (int i = 1; i < current_word_list.size(); i++) {
            tmp = tmp.concat(Function.giveLetter( (int)current_word_list.get(i)));
          }
          tmp = tmp.concat(Function.giveLetter(keyboard_input-2));
          current_word_str = tmp;
        }
        // Si premier mot, on met la première lettre en maj
        current_word_str = render_case(current_word_str);

        current_word_list.add(keyboard_input-2);
        updateScreen();
      }

      // suiv. = passer à la suggestion suivante
      else if (keyboard_input == 1) {
        // On décale toute la liste, le premier mot passe en dernier
        tree.switchWords();
        if (tree.getWords().get(0) != " "){
          // On met à jour l'affichage
          current_word_str = (String)tree.getWords().get(0);
        }

        current_word_str = render_case(current_word_str);
        updateScreen();
      }

      // <- = effacer dernier caractère
      else if (keyboard_input == 2) {
        // Deux cas :
        // * Soit on efface le mot courant, et dans ce cas on 'remonte' dans l'arbre
        // bon en fait on refait la recherche à partir du début...
        if ( current_word_list.size() > 0 ) {
          // On enlève le dernier chiffre tapé
          current_word_list.remove( current_word_list.size()-1 );
        }
        // * Soit on efface les mots d'avant,
        else if (phrase.length() > 0) {
          phrase = phrase.substring(0, phrase.length()-1);
          phrase_list.remove(phrase_list.size()-1);
          // si le dernier caractère est une lettre, on recupère le mot d'avant
          if (!phrase_list.isEmpty() && phrase_list.get(phrase_list.size()-1) != 12 && phrase_list.get(phrase_list.size()-1) != 1) {
            // on récupère tout dans les variables correspondant au mot courrant
            for (int i =phrase_list.size()-1; i>=0;i-- ) {
              if (phrase_list.get(i) > 1 && phrase_list.get(i) <10) /* si c'est une lettre */ {
                current_word_list.add(0,phrase_list.get(i));
                // on supprime au fur et à mesure des variables de phrase
                phrase = phrase.substring(0, phrase.length()-1);
                phrase_list.remove(phrase_list.size()-1);
              }else{
                break;
              }
            }
          }
        }

          // On relance la recherche
          tree = root;
          if (current_word_list.size() > 0) {
            for (int i = 0; i<current_word_list.size(); i++){
              if (tree.getEqkid() != null) {
                tree = tree.getEqkid().search( (int)current_word_list.get(i) );
              }else{ //cas où on cherche à atteindre une séquence qui est trop profonde et sort de l'arbre
                String tmp = Function.giveLetter( (int)current_word_list.get(0));
                for (int j = 1; j < current_word_list.size(); j++) {
                  tmp = tmp.concat(Function.giveLetter( (int)current_word_list.get(j)));
                }
                current_word_str = tmp;
                break;
              }
            }
            if ((String)tree.getWords().get(0) == " ") { // pour afficher tout de même qqchose si le noeud ne contient pas de mots
                String tmp = Function.giveLetter( (int)current_word_list.get(0));
                for (int i = 1; i < current_word_list.size(); i++) {
                  tmp = tmp.concat(Function.giveLetter( (int)current_word_list.get(i)));
                }
                current_word_str = tmp;
            }else{
              current_word_str = (String)tree.getWords().get(0);
            }

            current_word_str = render_case(current_word_str);
          }
          else
            current_word_str = "";

        current_word_str = render_case(current_word_str);
        // Update de l'affichage !
        updateScreen();
      }

      // # = espace
      else if (keyboard_input == 14) {
        phrase = phrase.concat(current_word_str).concat(" ");
        current_word_str = "";
        phrase_list.addAll(current_word_list);
        phrase_list.add(keyboard_input-2);
        current_word_list.clear();
        updateScreen();
        tree = root;
        // On passe en ponctuation normale si on était en sur le premier mot
        if (case_mode == 0)
          case_mode = 1;
      }

      // * = changer la casse
      else if (keyboard_input == 12) {
        // case_mode = 0 <=> Bla, case_mode = 1 <=> bla, case_mode = 2 <=> BLA
        case_mode++;
        case_mode %= 3;
        current_word_str = render_case(current_word_str);
        updateScreen();
        // change key_button[10]
      }

      // 1 = Ponctuation
      else if (keyboard_input == 3) {
        // Termine le mot s'il on en écrivait un
        if (current_word_list.size() > 0) {
          phrase = phrase.concat(current_word_str);
          current_word_str = "";
          phrase_list.addAll(current_word_list);
          phrase_list.add(keyboard_input-2);
          current_word_list.clear();
          tree = root;
        }
        // Ajout du signe de ponctuation suivant
        // Si c'est la première fois qu'on appuie sur la touche,
        // on ajoute le premier signe
        if (last_keyboard_input != 3) {
          ponctuation_idx = 0;
          phrase = phrase.concat( ponctuation[ponctuation_idx % ponctuation.length] );
        }
        // sinon, on remplace le signe de ponctuation
        else{
          phrase = phrase = phrase.substring(0, phrase.length()-1);
          ponctuation_idx++;
          phrase = phrase.concat( ponctuation[ponctuation_idx % ponctuation.length] );
        }
        updateScreen();
        // Premier mots => capitalize le prochain mot
        case_mode = 0;
      }

      last_keyboard_input = keyboard_input;
    }
  }
}

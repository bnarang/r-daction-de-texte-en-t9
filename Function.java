public class Function {
  //Obtenir le chiffre du clavier "téléphone" correspondant au caractère letter
  public static int giveCorrespondingNumber(char letter) {

    int res = -1;

    switch (letter){
      case 'a': case 'b': case 'c':           case 'A': case 'B': case 'C':           res = 2; break;
      case 'd': case 'e': case 'f':           case 'D': case 'E': case 'F':           res = 3; break;
      case 'g': case 'h': case 'i':           case 'G': case 'H': case 'I':           res = 4; break;
      case 'j': case 'k': case 'l':           case 'J': case 'K': case 'L':           res = 5; break;
      case 'm': case 'n': case 'o':           case 'M': case 'N': case 'O':           res = 6; break;
      case 'p': case 'q': case 'r': case 's': case 'P': case 'Q': case 'R': case 'S': res = 7; break;
      case 't': case 'u': case 'v':           case 'T': case 'U': case 'V':           res = 8; break;
      case 'w': case 'x': case 'y': case 'z': case 'W': case 'X': case 'Y': case 'Z': res = 9; break;
    }

    return res;
  }

  //Obtenir la première lettre correspondant au chiffre number
  public static String giveLetter(int number) {
    String res = "a";

    switch (number){
      case 2: res = "a"; break;
      case 3: res = "d"; break;
      case 4: res = "g"; break;
      case 5: res = "j"; break;
      case 6: res = "m"; break;
      case 7: res = "p"; break;
      case 8: res = "t"; break;
      case 9: res = "w"; break;
    }

    return res;
  }
}

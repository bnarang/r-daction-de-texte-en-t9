import java.io.*;
import java.util.ArrayList;


public class main {
  public static void main(String[] args) {
    try {
      // ouverture du fichier dictionnaire
      System.out.println("Dictionary initalization...");
      FileInputStream dicofile = new FileInputStream("randdico.txt");
      BufferedReader  buff     = new BufferedReader( new InputStreamReader(dicofile) );
      String line;
      // lecture du dictionnaire et initialisation de l'arbre racine
      Tree root    = new Tree(5);
      while ( (line = buff.readLine()) != null )
        root.getEqkid().addWord(line, 0);
      buff.close();

      // Lancement de la fenêtre
      // Tout le programme se déroule dans MainWindow, qui a une copie de root
      System.out.println("Completed ! Now starting main window.");
      MainWindow main_window = new MainWindow(root);
    }

    catch (IOException e) {
      System.err.println("Error: " + e.getMessage());
    }
  }
}

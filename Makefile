JFLAGS = -g
JC = javac
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
	Function.java \
	MainWindow.java \
	main.java \
	Tree.java

default: classes

classes: $(CLASSES:.java=.class)

clean:
	$(RM) *.class

